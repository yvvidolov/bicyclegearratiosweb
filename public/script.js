
// const h4 = document.createElement('h4')
// body.append(h4) 
// h4.innerHTML = "hello javascript"

var bikeTable;
let bikes = [];

bikes.push(
		{
		 "name": "Default 3x10",
		 "mid_low": "24",
		 "mid_high": "42",
		 "back_low": "11",
		 "back_high": "36",
		 "wheel_size": "29"
		}
	);
bikes.push(
		{
		 "name": "Default 1x11",
		 "mid_low": "34",
		 "mid_high": "34",
		 "back_low": "10",
		 "back_high": "42",
		 "wheel_size": "29"
		}
	);
for(var i = 0; i < 8; ++i) {
	bikes.push(
			{
			 "name": "",
			 "mid_low": "",
			 "mid_high": "",
			 "back_low": "",
			 "back_high": "",
			 "wheel_size": ""
			}
		);
}

function addHeader()
{
	var row, cell;

	row = bikeTable.insertRow();
	cell = row.insertCell();
	cell.innerHTML = "Name";
	cell.rowSpan = 2;
	cell = row.insertCell();
	cell.innerHTML = "Wheel";
	cell.rowSpan = 2;
	cell = row.insertCell();
	cell.innerHTML = "Middle";
	cell.colSpan = 2;
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "Cassette";
	cell.className = 'dd';
	cell.colSpan = 2;
	cell = row.insertCell();
	cell.innerHTML = "Ratio";
	cell.colSpan = 2;
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "Cadence (low gear)(km/h)";
	cell.className = 'dd';
	cell.colSpan = 3;
	cell = row.insertCell();
	cell.innerHTML = "Cadence (high gear)(km/h)";
	cell.className = 'dd';
	cell.colSpan = 3;

	row = bikeTable.insertRow();
	row.className = 'db';
	cell = row.insertCell();
	cell.innerHTML = "Low";
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "High";
	cell = row.insertCell();
	cell.innerHTML = "Low";
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "High";
	cell = row.insertCell();
	cell.innerHTML = "Lowest";
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "Highest";

	cell = row.insertCell();
	cell.innerHTML = "60";
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "90";
	cell = row.insertCell();
	cell.innerHTML = "120";
	cell = row.insertCell();
	cell.innerHTML = "60";
	cell.className = 'dd';
	cell = row.insertCell();
	cell.innerHTML = "90";
	cell = row.insertCell();
	cell.innerHTML = "120";
}

function addRow(id)
{
	var row, cell;
	row = bikeTable.insertRow();

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='20' size='10' id='in"+id+"0' value='"+bikes[id].name+"' onkeydown='parse()' />";

	if(bikes[id].name == "")
	{
		cell.colSpan = 14;
		cell.style = "text-align: left;"
		return;
	}

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='3' size='2' id='in"+id+"1' value='"+bikes[id].wheel_size+"' onkeydown='parse()' />";

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='3' size='2' id='in"+id+"2' value='"+bikes[id].mid_low+"' onkeydown='parse()' />";
	cell.className = 'dd';

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='3' size='2' id='in"+id+"3' value='"+bikes[id].mid_high+"' onkeydown='parse()' />";

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='3' size='2' id='in"+id+"4' value='"+bikes[id].back_low+"' onkeydown='parse()' />";
	cell.className = 'dd';

	cell = row.insertCell();
	cell.innerHTML = "<input type='text' maxlength='3' size='2' id='in"+id+"5' value='"+bikes[id].back_high+"' onkeydown='parse()' />";

	var ratioLowest = parseFloat(bikes[id].mid_high)/parseFloat(bikes[id].back_low);
	var ratioHighest = parseFloat(bikes[id].mid_low)/parseFloat(bikes[id].back_high);

	cell = row.insertCell();
	cell.innerHTML = (ratioLowest).toPrecision(2).toString();
	cell.className = 'dd';

	cell = row.insertCell();
	cell.innerHTML = (ratioHighest).toPrecision(2).toString();

	var wheelCircumference = Math.PI*bikes[id].wheel_size*2.54/100;

	cell = row.insertCell();
	cell.innerHTML = (60*wheelCircumference*ratioHighest*60/1000).toPrecision(2).toString();
	cell.className = 'dd';

	cell = row.insertCell();
	cell.innerHTML = (90*wheelCircumference*ratioHighest*60/1000).toPrecision(2).toString();

	cell = row.insertCell();
	cell.innerHTML = (120*wheelCircumference*ratioHighest*60/1000).toPrecision(2).toString();

	cell = row.insertCell();
	cell.innerHTML = (60*wheelCircumference*ratioLowest*60/1000).toPrecision(2).toString();
	cell.className = 'dd';

	cell = row.insertCell();
	cell.innerHTML = (90*wheelCircumference*ratioLowest*60/1000).toPrecision(2).toString();

	cell = row.insertCell();
	cell.innerHTML = (120*wheelCircumference*ratioLowest*60/1000).toPrecision(2).toString();
	
}

function buildTable()
{
	bikeTable = document.getElementById("bikeTable");
	bikeTable.textContent = '';

	addHeader();
	for(var i = 0; i < bikes.length; ++i) {
		addRow(i);
		if(bikes[i].name == "")
			break;
	}
}

function parse()
{
	if(event.keyCode != 13) return;

	for(var i = 0; i < bikes.length; ++i) {
		input = document.getElementById("in"+i+"0");
		if(!input) break;
		bikes[i].name = input.value;

		input = document.getElementById("in"+i+"1");
		if(!input) break;
		bikes[i].wheel_size = input.value;

		input = document.getElementById("in"+i+"2");
		bikes[i].mid_low = input.value;

		input = document.getElementById("in"+i+"3");
		bikes[i].mid_high = input.value;

		input = document.getElementById("in"+i+"4");
		bikes[i].back_low = input.value;

		input = document.getElementById("in"+i+"5");
		bikes[i].back_high = input.value;
	}

	buildTable();
}

function toUrl()
{
	var url = window.location.href.split('?')[0]+"?";

	var string="";
	for(var i = 0; i < bikes.length; ++i) {
		if(bikes[i].name == "") break;

		if(i != 0) string += "|"
		string += bikes[i].name.replaceAll(',', '').replaceAll('|', '') + ",";
		string += bikes[i].mid_low.replaceAll(',', '').replaceAll('|', '') + ",";
		string += bikes[i].mid_high.replaceAll(',', '').replaceAll('|', '') + ",";
		string += bikes[i].back_low.replaceAll(',', '').replaceAll('|', '') + ",";
		string += bikes[i].back_high.replaceAll(',', '').replaceAll('|', '') + ",";
		string += bikes[i].wheel_size.replaceAll(',', '').replaceAll('|', '');

	}

	share = document.getElementById("share");
	share.value = url+encodeURIComponent(string);
	share.select();

}

window.addEventListener("load", function(){
	const urlParams = decodeURIComponent(window.location.search).substring(1);
	var bikesString = urlParams.split('|')
	for(var i = 0; i < bikesString.length; ++i) {
		if(i > bikes.length) break;
		bikeString = bikesString[i].split(',')
		if(bikeString.length != 6) continue;

		bikes[i].name = bikeString[0];
		bikes[i].mid_low = bikeString[1];
		bikes[i].mid_high = bikeString[2];
		bikes[i].back_low = bikeString[3];
		bikes[i].back_high = bikeString[4];
		bikes[i].wheel_size = bikeString[5];
	}
	

	buildTable();
});
